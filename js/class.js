function Frontend()
{
	//Private Attribute
	//Path para configurar json ruta
	var pathJson='json/news_mock.json';  
	//Contiene el resultado del json :D
	var resultJson;  
	// private constructor 
   	var __construct = function() 
   	{
   		$('#boxMenu').click(function()
		{
			renderListNews();
		});
   	}();
	//Anima la cajas
	var animateBoxes=function()
	{
	
		$.each($('.boxMini'), function(i, field)
		{
			var speed=Math.floor((Math.random() * 1000) + 1000);
			$(this).animate
			({
				left: "+=50",
				width: [ "toggle", "easeOutBounce" ],
			}, speed, function()
			{
				//Animate complete
			});		
		});
	}
	//Crea y renderiza las cajas
	var createBox=function()
	{				
		$.each(resultJson, function(i, field)
		{
			var string='';
			string+='<div data-title="'+field.title+'" data-detail="'+i+'" id="boxMini'+i+'" class="boxMini">';
			string+='<div class="boxImgThumb">';
			string+='<div class="simulateImg"></div>';
			string+='</div>';
			string+='<div class="boxTitle">'+field.title+'</div>';
			string+='</div>';	
			string+='<div id="boxDetail'+i+'" class="boxDetail">';
			string+='<div class="boxImg"><img id="img'+i+'" alt="'+field.title+'" title="'+field.title+'"/></div>';
			string+='<div class="boxDetailRight">';
			string+='<div class="boxTitle">'+field.title+'</div>';
			string+='<div class="boxDescription">'+field.content+'</div>';
			string+='</div>';
			string+='</div>';	
			$('#boxContent').html($('#boxContent').html()+string);	
			var $image = $("#img"+i);
			var $downloadingImage = $("#img"+i);
			$downloadingImage.load(function()
			{
			  	$image.attr("src", $(this).attr("src"));	
			});
			$downloadingImage.attr("src",field.image);
		});
		
	}
	//Le coloca los eventos a las cajas cuando realizan click
	var eventBoxClick=function()
	{
		$('.boxMini').click(function() 
		{
			$('#descSelected').html($(this).data('title'));
			$('#boxDetail'+$(this).data('detail')).slideToggle(500,"easeOutBounce",function()
			{			
				if($(this).css('display')=='block')
				{
					$('.textHeader')				
					.css('marginLeft','5%')
					.css('opacity',1)				
					.slideDown(700,"easeOutBounce")	
				}
				else
				{
					$('.textHeader')
					.css('marginLeft','5%')		
					.animate(
					{
						opacity: 0,
						marginLeft: "-=100",
					}, 500, function() 
					{
						$(this).slideUp();
					});
				}
	        });
		});
	}
	
	//Renderiza la lista de los elementos cuando son buscados por el getJson
	var renderListNews=function()
	{
		$.getJSON(pathJson, function(result)
		{
			resultJson=result;
			createBox();
			animateBoxes();
			eventBoxClick();
	    });  
	}	
}